# Word-by-word Merge GUI project
GUI ([simple web page](https://stranskyjan.gitlab.io/word-by-word-merge-gui)) to easily merge (and possibly further edit) plain text documents "word-by-word" (not only "line-by-line" as is usual).

#### Demo
![demo](demo.gif)

Tip: use keyboard shortcuts instead of clicking

## Usage
#### Browser
- click `File 1` and `File 2` buttons to load the files
- accept / reject changes and/or edit the document
- click `Save` button (or press `ctrl s`) to save the result (optionally change pre-selected file name)

#### Command line
Same as Browser section above, just the browser is started from command line and the page pre-load files specified as command line arguments.
See [`cli`](cli) directory for more details.

#### Changes, accepting / rejecting, jumps between changes
- put cursor to the "red" or "green" change.
- click `Accept` button (or press `shift +`, the `+` being on NumPad)
- click `Reject` button (or press `shift -`, the `-` being on NumPad)
- click `Next` button (or press `shift PgDn`) to jump to the next change
- click `Prev` button (or press `shift PgUp`) to jump to the previous change

#### Editing
The text is editable.
Ordinary text remains ordinary, changes within "green" or "red" areas becomes green or red, too.
Changes are un/re-doable, clicking `Undo` or `Redo` button, or pressing `ctrl z` or `ctrl y`, respectively.

## Compatibility
Tested with Google Chrome 93

## Background
Main components:
- Myers diff algorithm
- `contenteditable` attribute of content HTML element
- gluing JavaScript

## Contribution
is welcome :-)

#### Bug reporting, wishes, ...
In case of any question, problem, wish, ... please leave an issue at the [GitLab page of the project](https://gitlab.com/stranskyjan/word-by-word-merge-gui)

#### Contributors
- [Jan Stránský](https://gitlab.com/stranskyjan)

## TODO
- cursor position after undo/redo
- demo
