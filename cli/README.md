# CLI (Command Line Interface) to Word-by-word Merge GUI program
Command line app (shell script):
- launches http server
- launches browser
- browser pre-load files specified on the command line

## Browsers
Currently only script for Google Chrome is present, see [google-chrome](google-chrome) directory
