# word-by-word-merge-gui-google-chrome
CLI (Command Line Interface) to Word-by-word Merge GUI program for Google Chrome browser.

Command line app (shell script) launches http server and Google Chrome with pre-loaded specified files.

## Usage
```
word-by-word-merge-gui-google-chrome /path/to/file1 /path/to/file2
```

## Installation
```
make install
```
default install location is `/usr/local`. To use a different location (e.g. `$HOME`), use
```
make install PREFIX=/path/to/your/desired/install/directory
```

#### Prerequisites
- Google Chrome browser (obviously)
- Python 3, for server (using `python3 -m http.server` as the default one). A different (e.g. `npm`) server could be used instead.

## Compatibility
Tested on:
- browser: Google Chrome 93
- operating system: Ubuntu 18.04 LTS

## Additional info
The shell script also creates a temporary directory `/tmp/word-by-word-merge-gui-google-chrome-tmp-dir` containing the `word-by-word-merge-gui` page source code as well as copy of the given files.

Google Chrome is started as `--app` with `--user-data-dir=/tmp/word-by-word-merge-gui-google-chrome-user-data-dir` and `--no-first-run` options.
