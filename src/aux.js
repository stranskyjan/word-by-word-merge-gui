/**
 * @template A,B
 * @param {A[]} a1
 * @param {B[]} a2
 */
export function zip(a1,a2) {
	/** @type {[A,B][]} */
	var ret = []
	for (var i=0; i<a1.length; i++) {
		ret.push([a1[i],a2[i]])
	}
    return ret
}

/**
 * @param {number} a
 * @param {number} [b]
 * @param {number} [c]
 */
export function range(a,b,c=1) {
	var ret = []
	if (b === undefined) {
		b = a
		a = 0
	}
	for (var v=a; v<b; v+=c) ret.push(v)
	return ret
}

/**
 * @param {string} str
 */
export function split(str) {
	var re = /(\p{L}+|[\s,;.?!]+|.+?|^$)/gu
	var m = str.match(re)
	return Array.from(m)
}

/**
 * @template T
 * @param {T[]} array
 */
export function last(array) {
	return array[array.length-1]
}
