// @ts-check
/*
 * based on
 * https://github.com/rec/myers
 */
import {range} from "./aux.js"

/**
 * @typedef {KEEP|INSERT|REMOVE} DiffType
 * @typedef {[DiffType,string|[string,string]]} Diff
 */

export const KEEP = "k"
export const INSERT = "i"
export const REMOVE = "r"

const equalityDiffLimitRatio = 1

/**
 * @param {string} e1
 * @param {string} e2
 */
function equals(e1,e2,strict=false) {
	if (strict) return e1 === e2
    if (e1 === "" && e2 === "") return true
	var df = diff(e1.split(""),e2.split(""),true)
	var ddf = df.filter(d => d[0] === INSERT || d[0] === REMOVE)
	return ddf.length < equalityDiffLimitRatio * Math.max(e1.length,e2.length)
}

/**
 * @param {string[]} a
 * @param {string[]} b
 */
export function diff(a,b,strict=false) {
	/** @type {Object<number,[number,Diff[]]>}  */
    var front = {1: [0, []]}
    for (var d of range(0, a.length + b.length + 1)) {
        for (var k of range(-d, d + 1, 2)) {
            var go_down = k === -d || (k !== d && front[k - 1][0] < front[k + 1][0])
            if (go_down) {
                var [old_x, history] = front[k + 1]
                var x = old_x
			} else {
                var [old_x, history] = front[k - 1]
                var x = old_x + 1
			}
            var y = x - k
            history = history.slice()
            if (1 <= y && y <= b.length && go_down) {
                history.push([INSERT, b[y - 1]])
			} else if (1 <= x && x <= a.length) {
                history.push([REMOVE, a[x - 1]])
			}
            while (x < a.length && y < b.length && equals(a[x],b[y],strict)) {
                x += 1
                y += 1
                history.push([KEEP, [a[x-1],b[y-1]]])
			}
            if (x >= a.length && y >= b.length) {
                return history
			}
            front[k] = [x, history]
		}
	}
}

export default diff
