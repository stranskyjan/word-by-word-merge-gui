// @ts-check
import {zip,split,last} from "./aux.js"
import {diff,KEEP,INSERT,REMOVE} from "./diff.js"

const ids = {
	content: "content",
	buttonFile1: "button-file1",
	buttonFile2: "button-file2",
	buttonSave: "button-save",
	downloadFileName: "download-file-name",
	save: "save",
	diffButtons: "diff-buttons",
	buttonNext: "button-next",
	buttonPrev: "button-prev",
	buttonAccept: "button-accept",
	buttonReject: "button-reject",
	buttonUndo: "button-undo",
	buttonRedo: "button-redo",
	historyButtons: "history-buttons"
}
/** @param {string} id */
function byId(id) { return document.getElementById(id) }

const classes = {
	remove: "remove",
	insert: "insert",
	line: "line",
}

var fileContents = new Array(2).fill()
var fileNames = new Array(2).fill()

function error(msg="TODO") {
	alert(msg)
	throw new Error(msg)
}

/** @param {number} i */
function loadUserFile(i) {
	var input = document.createElement('input')
	input.type = "file"
	input.accept = "text/*"
	input.onchange = e => {
		var target = /** @type {HTMLInputElement} */ (e.target)
		var file = target.files[0];
		applyFileName(file.name,i)
		var reader = new FileReader()
		reader.onloadend = () => {
			var fileContent = /** @type {string} */ (reader.result)
			applyFileContent(fileContent,i)
		}
		reader.readAsText(file)
	}
	input.click()
}

function loadAddressFiles() {
	var params = new URLSearchParams(window.location.search)
	var fileNames = ["file1","file2"].map(k => params.get(k)).map(f => `${f}`)
	fileNames.forEach((fn,i) => {
		applyFileName(fn,i)
		fetch(fn).then(response => response.text()).then(content => applyFileContent(content,i))
	})
}

/**
 * @param {string} fileContent
 * @param {number} i
 */
function applyFileContent(fileContent,i) {
	fileContent = fileContent.replace(/^\n*/,"").replace(/\n*$/,"")
	var lines = fileContent.split("\n")
	fileContents[i] = lines
	if (fileContents.every(Boolean)) setTimeout(()=>start())
}

/**
 * @param {string} fileName
 * @param {number} i
 */
function applyFileName(fileName,i) {
	fileNames[i] = fileName
	var id = i === 0 ? ids.buttonFile1 : ids.buttonFile2
	var button = /** @type {HTMLButtonElement} */ (byId(id))
	button.disabled = true
	button.nextElementSibling.innerHTML = fileName
}

function loadUserFile1() { loadUserFile(0) }
function loadUserFile2() { loadUserFile(1) }

function save() {
	var content = byId(ids.content)
	var elines = Array.from(content.children)
	var tlines = elines.map(eline => (/**@type{HTMLDivElement}*/(eline)).innerText)
	var text = tlines.join("\n") + "\n"
	var blob = new Blob([text],{type:"text/plain"})
	//
	var filename = byId(ids.downloadFileName).innerText
	var a = document.createElement("a")
 	a.href = window.URL.createObjectURL(blob)
	a.setAttribute("download",filename)
	a.click();
	window.URL.revokeObjectURL(a.href);
	history.savedHTML = content.innerHTML
}

function fillContent() {
	var elem = byId(ids.content)
	var [c1,c2] = fileContents
	var dLines = diff(c1,c2)
	for (var [type,str] of dLines) {
		var div = document.createElement("div")
		div.classList.add(classes.line)
		elem.appendChild(div)
		if (type === KEEP) {
			str = /** @type {[string,string]} */ (str)
			var [split1,split2] = str.map(split)
			var dLine = diff(split1,split2,true)
			var curType = undefined
			for (var [t,s] of dLine) {
				if (curType !== t) {
					curType = t
					var span = document.createElement("span")
					div.appendChild(span)
					if (t === REMOVE) span.classList.add(classes.remove)
					if (t === INSERT) span.classList.add(classes.insert)
				}
				if (t === KEEP) s = s[0]
				span.innerHTML += s
			}
		} else {
			str = /** @type {string} */ (str)
			var span = document.createElement("span")
			div.appendChild(span)
			span.innerHTML = str
			if (type === INSERT) {
				span.classList.add(classes.insert)
			} else if (type === REMOVE) {
				span.classList.add(classes.remove)
			}
		}
	}
}

function start() {
	var content = byId(ids.content)
	content.style.visibility = "visible"
	content.contentEditable = "true"
	fillContent()
	//
	var bsave = /** @type {HTMLButtonElement} */ (byId(ids.buttonSave))
	bsave.disabled = false
	//
	var doenloadFileName = byId(ids.downloadFileName)
	var fname = last(fileNames[0].split(/[\/\\]/))
	doenloadFileName.innerHTML = fname
	doenloadFileName.contentEditable = "true"
	//
	var showIds = [ids.save,ids.diffButtons,ids.historyButtons]
	showIds.forEach(id => {
		var b = byId(id)
		b.style.visibility = "visible"
	})
	//
	history.done[0] = content.innerHTML
}

/** @param {"n"|"p"} direction */
function findDiff(direction) {
	var c = byId(ids.content)
	var selection = document.getSelection()
	var spans = Array.from(c.querySelectorAll("span"))
	var n = selection.anchorNode
	if (!n || !c.contains(n)) var parent = /** @type {HTMLElement} */ (direction === "p" ? c.firstElementChild.firstElementChild : c.lastElementChild.lastElementChild)
	else var parent = selection.anchorNode.parentElement
	while (parent instanceof HTMLDivElement) parent = /** @type {HTMLElement} */ (parent.firstElementChild)
	if (!(parent instanceof HTMLSpanElement)) error(parent.toString())
	var i = spans.findIndex(e => e === parent)
	if (i === -1) error()
	while (true) {
		i = direction === "p" ? i-1 : i+1
		i = (i+spans.length) % spans.length
		var span = spans[i]
		if (span.classList.contains(classes.remove) || span.classList.contains(classes.insert)) break
		if (span === parent) break
	}
	if (span === parent) {
		alert("no more diff!")
		return
	}
	//
	var range = document.createRange()
	var fch = span.firstChild
	range.setStart(fch,0)
	range.setEnd(fch,fch.textContent.length)
	selection.removeAllRanges()
	selection.addRange(range)
	fch.parentElement.scrollIntoView({behavior:"smooth",block:"center"})
}
function findNextDiff() { return findDiff("n") }
function findPrevDiff() { return findDiff("p") }

function accept() {
	var selection = document.getSelection()
	var n = selection.anchorNode
	if (!n) return
	var c = byId(ids.content)
	if (!c.contains(n)) return
	n.parentElement.classList.remove(classes.remove,classes.insert)
	c.focus()
	history.snap()
}
function reject() {
	var selection = document.getSelection()
	var n = selection.anchorNode
	if (!n) return
	var c = byId(ids.content)
	if (!c.contains(n)) return
	var p = n.parentElement
	if (!p.classList.contains(classes.remove) && !p.classList.contains(classes.insert)) return
	/** @type {HTMLElement} */
	var e
	var index = 0
	if (p.nextElementSibling) {
		e = /** @type {HTMLElement} */ (p.nextElementSibling)
	} else if (p.previousElementSibling) {
		e = /** @type {HTMLElement} */ (p.previousElementSibling)
		index = e.textContent.length
	} else {
		e = p.parentElement
	}
	p.parentElement.removeChild(p)
	var range = document.createRange()
	while (!e.firstChild) e = e.parentElement
	range.setStart(e.firstChild,index)
	selection.removeAllRanges()
	selection.addRange(range)
	history.snap()
}

var history = {
	/** @type {string[]} */
	done: [],
	/** @type {string[]} */
	undone: [],
	savedHTML: "",
	last() {
		return this.done[this.done.length-1]
	},
	apply() {
		var html = this.last()
		var c = byId(ids.content)
		c.innerHTML = html
	},
	snap() {
		var c = byId(ids.content)
		var chtml = c.innerHTML
		var lhtml = this.last()
		if (chtml !== lhtml) {
			this.done.push(chtml)
			this.undone = []
		}
	},
}
function undo() {
	history.snap()
	if (history.done.length <= 1) return
	var h = history.done.pop()
	history.undone.push(h)
	history.apply()
}
function redo() {
	if (history.undone.length === 0) return
	var h1 = history.undone.pop()
	history.done.push(h1)
	history.apply()
}

window.addEventListener("load", () => {
	// add click event listeners to buttons
	var {buttonFile1,buttonFile2,buttonSave,buttonNext,buttonPrev,buttonAccept,buttonReject,buttonUndo,buttonRedo} = ids
	var buttons = [buttonFile1,buttonFile2,buttonSave,buttonNext,buttonPrev,buttonAccept,buttonReject,buttonUndo,buttonRedo].map(id => byId(id))
	var callbacks = [loadUserFile1,loadUserFile2,save,findNextDiff,findPrevDiff,accept,reject,undo,redo]
	for (var [button,callback] of zip(buttons,callbacks)) button.addEventListener("click",callback)
	window.addEventListener("keydown", e => {
		if (e.code === "NumpadAdd" && e.shiftKey) {
			e.preventDefault()
			accept()
		}
		if (e.code === "NumpadSubtract" && e.shiftKey) {
			e.preventDefault()
			reject()
		}
		if (e.code === "PageDown" && e.shiftKey) {
			e.preventDefault()
			findNextDiff()
		}
		if (e.code === "PageUp" && e.shiftKey) {
			e.preventDefault()
			findPrevDiff()
		}
		if (e.code === "KeyS" && e.ctrlKey) {
			e.preventDefault()
			save()
		}
		if (e.code === "KeyZ" && e.ctrlKey) {
			e.preventDefault()
			undo()
		}
		if (e.code === "KeyY" && e.ctrlKey) {
			e.preventDefault()
			redo()
		}
		if (e.code.match(/^Arrow(?:Up|Down|Left|Right)$/)) {
			history.snap()
		}
	})
	var content = byId(ids.content)
	var contentEventsForSnap = ["blur","mousedown","touchstart"]
	contentEventsForSnap.forEach(e => content.addEventListener(e, () => history.snap()))
	window.addEventListener("beforeunload", e => {
		var c = byId(ids.content)
		var html = c.innerHTML
		if (html === history.savedHTML) return undefined
		return e.returnValue = true
	})
	//
	loadAddressFiles()
})
