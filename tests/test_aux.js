import assert from "assert"
import {range,zip,split} from "../src/aux.js"

describe("aux", () => {
	it("range", () => {
		assert.deepEqual(range(4),[0,1,2,3])
		assert.deepEqual(range(2,5),[2,3,4])
		assert.deepEqual(range(3,8,2),[3,5,7])
	})
	it("zip", () => {
		assert.deepStrictEqual(zip([1,2,3],["a","b","c"]),[[1,"a"],[2,"b"],[3,"c"]])
	})
	it("split", () => {
		assert.deepStrictEqual(split("lorem ipsum"),["lorem"," ","ipsum"])
		assert.deepStrictEqual(split("lorem,ipsum"),["lorem",",","ipsum"])
		assert.deepStrictEqual(split("lorem, ipsum"),["lorem",", ","ipsum"])
		assert.deepStrictEqual(split("lorem , ipsum"),["lorem"," , ","ipsum"])
		assert.deepStrictEqual(split("žluťoučký kůň"),["žluťoučký"," ","kůň"])
		assert.deepStrictEqual(split("žluťoučký,kůň"),["žluťoučký",",","kůň"])
		assert.deepStrictEqual(split("žluťoučký, kůň"),["žluťoučký",", ","kůň"])
	})
})
