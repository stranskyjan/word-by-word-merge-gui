import assert from "assert"
import {diff,KEEP as K,INSERT as I, REMOVE as R} from "../src/diff.js"

describe("diff", () => {
	const r = s => [R,s]
	const i = s => [I,s]
	const k = (s1,s2) => [K,[s1,s2??s1]]
	describe("simple strings", () => {
		const d = (s1,s2) => diff(s1.split(""),s2.split(""))
		const f = (str,diff) => str.split("").map((e,index) => {
			var d = diff[index]
			var t = d === "+" ? i : d === "-" ? r : k
			return t(e,e)
		})
		it("basics", () => {
			assert.deepStrictEqual(d("abc","abc"),f("abc","   "))
			assert.deepStrictEqual(d("abc",""),f("abc","---"))
			assert.deepStrictEqual(d("","abc"),f("abc","+++"))
			assert.deepStrictEqual(d("xyabc","abc"),f("xyabc","--   "))
			assert.deepStrictEqual(d("abcxy","abc"),f("abcxy","   --"))
			assert.deepStrictEqual(d("abc","xyabc"),f("xyabc","++   "))
			assert.deepStrictEqual(d("abc","abcxy"),f("abcxy","   ++"))
			assert.deepStrictEqual(d("abxc","abc"),f("abxc","  - "))
			assert.deepStrictEqual(d("abc","abxc"),f("abxc","  + "))
			assert.deepStrictEqual(d("abxyc","abmnc"),f("abxymnc","  --++ "))
		})
	})
	describe("diff", () => {
		assert.deepStrictEqual(diff(["a","b"],["a","b"]),[k("a"),k("b")])
		assert.deepStrictEqual(diff(["a","","b"],["a","","b"]),[k("a"),k(""),k("b")])
	})
})
